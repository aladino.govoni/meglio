MEGLIO
======
Project for the analysis of data acquired by fiber optic loops.

## Download/install
Basically you can clone the project in the directory you prefer (let's say
```PATH_TO_MEGLIO```) with
```
git clone https://gitlab.rm.ingv.it/aladino.govoni/meglio.git
```
or update it with
```
git  pull
```
from the project directory or use option ```-C PATH_TO_MEGLIO```.
(more info on [Git-Book](https://git-scm.com/book/en/v2)).

To install, simply add the path ```PATH_TO_MEGLIO/meglio/bin``` to your user ```PATH```.
All python script have a matching bash script (without the .py extension) that
will activate the conda environment and run the proper python script.

If you want to use the library, just link ```PATH_TO_MEGLIO/meglio/src/fdsntools```
folder to your project.

## Programs
### megliott
```
Compute the travel times of specified events using obspy.taup to specified
position.
Events can be specified using either:
 * eventID          : integer unique identifier as provided by EIDA databank
 * lat,lon,depth_km : tuple with receiver coordinats TODO
 * filename         : text file with the same info (possible support for
                      GPX and/or KML format) TODO
 ** Needs fairly recent obspy (>=1.0), python3 and docopt.

Usage:
	megliott.py [-v ...] [options] -s <position> <eventID> ...

Options:
  -s, --station <position>
                          set station position as lat,lon[,elev,depth]
  -f, --fdsn <fdsn>       set fdsn service [default: INGV]
  -m, --model <model>     select velocity model [default: ak135]
  -P, --P-arrivals <P-arrivals>
                          select p arrivals
                          [default: P,p,Pdiff,PP,PKiKP,PKIKP,Pn,Pg]
  -S, --S-arrivals <S-arrivals>
                          select s arrivals
                          [default: S,s,Sdiff,SS,SKiKS,SKIKS,Sn,Sg]
  -o, --outdir <outdir>   store results to outdir as json.
  -r, --ranges            output only arrival ranges for each event.
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

```
## Download
```
git clone https://gitlab.rm.ingv.it/aladino.govoni/meglio.git
```
## Test commandline
```
time src/megliott.py -vv -s 42.8514,13.5892 28137841 27776721 -r 28301941
```
## Conda installation
If you already have a
[Miniconda](https://docs.conda.io/en/latest/miniconda.html)/
[Anaconda](https://www.anaconda.com/distribution/)
 system installed, just cd in the meglio folder and create the meglio environment
 from the
 [meglio-environment.yml](./meglio-environment.yml) file.
```
conda env create -f meglio-environment.yml
```
Otherwise here is a step by step recipe for creating it from scratch:
* Download installer with `python 3.9`:
```
$ wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.10.3-Linux-x86_64.sh
```
* install
```
$ bash Miniconda3-py39_4.10.3-Linux-x86_64.sh
```
* exit shell, and log in again, now
```
(base) meglio@corsaro:~$ which python
/home/meglio/miniconda3/bin/python
(base) meglio@corsaro:~$ python
Python 3.9.5 (default, Jun  4 2021, 12:28:51)
[GCC 7.5.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
```
* create the environment
```
$ conda config --add channels conda-forge
$ conda create -n meglio python=3.9
$ conda activate meglio
```
* install few useful packages (first update conda):
```
$ conda update conda
$ conda install -c anaconda docopt
$ conda install -c conda-forge jupyterlab
$ conda install scipy --channel conda-forge
```
* install `obspy` and `cartopy`:
```
$ conda install obspy
$ conda install cartopy (or basemap)
$ conda install -c conda-forge basemap-data-hires

$ obspy-runtests

Running /home/meglio/miniconda3/envs/meglio/lib/python3.9/site-packages/obspy/scripts/runtests.py, ObsPy version '1.2.2'
...
...
Your test results have been reported and are available at: https://tests.obspy.org/114563/
```
* for better plots install also seaborn and seaborn-images:
```
conda install -c conda-forge seaborn seaborn-image
```
