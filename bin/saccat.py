#!/bin/bash
#
\. "$(conda info --base)/etc/profile.d/conda.sh" || return $?
conda activate meglio
#/usr/bin/env python ${0%/*}/../src/${0##*/}.py $*
# py scripts are already executable
${0%/*}/../src/${0##*/}.py $*
