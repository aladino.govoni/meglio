#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Usage:
    saccat.py [-v ...] [options] <sacFile>

Options:
  -H, --Header            just header values
  -f, --full-precision    print full precision values (usually are formatted %g)
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno
import os.path
import numpy as np
import logging
from docopt import docopt

from obspy import read

logging.basicConfig(format='# %(filename)s : %(message)s')
log = logging.getLogger()

def saccat(sacFile, head=False, precision=False):
    '''
    '''
    log.info("reading %s",sacFile)
    tr = read(sacFile, debug_headers=True)[0]
    log.info("%s",str(tr))

    if head:
        for key, val in tr.stats.sac.items():
            print(f'{key.upper():10s} {val}')
        print(f'START {tr.stats.starttime}')
    else:
        print('############ sac header content ##############################################')
        for key, val in tr.stats.sac.items():
            print('#',f'{key.upper():10s} {val}')
        print('#',f'START      {tr.stats.starttime}')

        if precision:
            np.savetxt(sys.stdout, tr.data) #, fmt="%g")
        else:
            np.savetxt(sys.stdout, tr.data, fmt="%g")

        # numpy.savetxt(sys.stdout, a, fmt='%.4f')
        #print("TEST", file=sys.stdout)

if __name__ == "__main__":

    try:
        cli = docopt(__doc__, version=__version__)

        if cli['--verbose'] >= 2:
                log.setLevel(logging.DEBUG)
        elif cli['--verbose'] <= 1:
            log.setLevel(logging.INFO)
        log.debug("cli : %s",' '.join(str(cli).split()))

        saccat(cli['<sacFile>'],cli['--Header'], cli['--full-precision'])

    except KeyboardInterrupt:
        log.warning("Program halted")
        sys.exit()
