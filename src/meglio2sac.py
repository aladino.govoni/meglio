#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Convert hdf5 INRIM interferometer data to sac.
Default name for the instrument in Ascoli POP is set to
    IV.ASCOL..HJZ
(just one instrument for now) but it can be changed from options.
Future implementation:
 * these fields should be set directly on hdf5 file.
 * hdf file should be multi trace with specific attributes like
    * starttime, sampling, channel name, location

Most general output tree is:
    outdir/tag/eventID
(see -t and -e options). Output files are named from station code and ISO 8601
starttime time stamp.

Usage:
    meglio2sac.py [-v ...] [options] <hdf5> ...

Options:
  -n, --name <staCode>    fully qualified station name
                          [default: IV.ASCOL..HJZ]
  -c, --coordinates <coordinates>
                          set station position as lat,lon[,elev_m,depth_m]
                          [default: 42.85122,13.5894,150]
  -t, --tag               adds a tag directory level to outdir, tag is taken
                          from the hdf filename (without extension).
  -e, --eventID           adds an eventID directory level to outdir.
  -a, --annotate          annotates event dir with hidden files containing
                          information on events and extracted data
  -o, --outdir <outdir>   store results to outdir
                          [default: .]
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.2.0"
__status__ = "Develop"

import sys, errno
#import os.path

import logging

from docopt import docopt
#from datetime import datetime

from fdsntools.dataformats import hdf2sac

#logging.basicConfig(format='# %(filename)s : %(message)s')
logging.basicConfig(format='# %(module)s.%(funcName)s() : %(message)s')
#logging.basicConfig(format='# %(module)s : %(message)s')
log = logging.getLogger()

def main():

    try:
        cli = docopt(__doc__, version=__version__)

        # set verbose level
        if cli['--verbose'] >= 2:
            log.setLevel(logging.DEBUG)
        elif cli['--verbose'] == 1:
            log.setLevel(logging.INFO)
        log.debug("cli : %s",' '.join(str(cli).split()))

        sac = hdf2sac(
            inFiles = cli['<hdf5>'],
            code = cli['--name'],
            coord = cli['--coordinates'],
            outdir = cli['--outdir'],
            tag = cli['--tag'],
            eventID = cli['--eventID'],
            annotate = cli['--annotate']
        )
        sac.saveAll()
        
    except KeyboardInterrupt:
        log.warning("Program halted")
        sys.exit()

if __name__ == "__main__":
    main()
