#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Manage an event list directory storing event list, additional info,
station metadata and waveforms.
The event list directory must be initialized and the config file edited to
match extraction criteria. When executed on an existing event dir, it will try
to download only missing event information,
station metadata and waveform data. Needs latest obspy and python3.
The event list directory must be initialized explicitly and the config file
edited to match extraction criteria.
Needs latest obspy and python3.

Usage:
  megliott.py [-v ...] [options] status
  megliott.py [-v ...] [-f] [-t <tag>] [-c config] [-p <project>] init
  megliott.py [-v ...] [-p <project>] status
  megliott.py [-v ...] [-s <stations>] process|add|set

Options:
  -p <project>, --project <project>
                          set project dir [default: .].
  -s <stations>, --stations <stations>
                          set station list
  -e <events>, --events <events>
                          set event list
  -f, --force             if project dir already exist, forces
                          initialization, all files are deleted and config
                          is kept.
  -c <config>, --config <config>
                          name of config file to use.
  -v, --verbose           increase verbosity.
  -t <tag>, --tag <tag>   set event list name [default: EventList].
  -h, --help              display this help and exit.
  --version               output version information and exit.

Commands:
  init                    Initialize project dir and check defaults.
  process                 Import/process additional info (stations, events)
  add                     Import additional info (stations, events) and add to
                          current
  set                     Import additional info (stations, events) and set
  status                  Show project status
  download                Download all events in list
"""

__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno
import os.path
#
LOG_TAG="fdsntools"
import logging
logging.basicConfig(format='# %(filename)s : %(message)s')
log = logging.getLogger(LOG_TAG)
#
from docopt import docopt
from datetime import datetime
#
import pickle
#
from fdsntools.project import EventMgr,invalidProjectDir
#
if __name__ == "__main__":
	opt = {}
	opt['cli'] = docopt(__doc__, version=__version__)
	#print(opt); sys.exit()

	opt['log']=LOG_TAG

	if opt['cli']['--verbose'] >= 2:
		log.setLevel(logging.DEBUG)
	else:
		if opt['cli']['--verbose'] == 1:
			log.setLevel(logging.INFO)

	try:
		EM = EventMgr(opt)
		EM.saveStatus(dump=(opt['cli']['--verbose'] >= 2))
	except invalidProjectDir as err:
		log.error("EXCEPTION : %s",err);
		pass
	except KeyboardInterrupt:
		log.warning("Program halted")
		sys.exit()
