#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Compute the travel times of specified events using obspy.taup to specified
position.
Events can be specified using either:
 * eventID          : integer unique identifier as provided by EIDA databank
 * lat,lon,depth_km : tuple with receiver coordinats [**TODO**]
 * filename         : text file with the same info (possible support for
                      GPX and/or KML format) [**TODO**]
 ** Needs fairly recent obspy (>=1.0), python3 and docopt.

Usage:
    megliott.py [-v ...] [options] -s <position> <eventID> ...

Options:
  -s, --station <position>
                          set station position as lat,lon[,elev,depth]
  -f, --fdsn <fdsn>       set fdsn service [default: INGV]
  -m, --model <model>     select velocity model [default: ak135]
  -P, --P-arrivals <P-arrivals>
                          select p arrivals
                          [default: P,p,Pdiff,PP,PKiKP,PKIKP,Pn,Pg]
  -S, --S-arrivals <S-arrivals>
                          select s arrivals
                          [default: S,s,Sdiff,SS,SKiKS,SKIKS,Sn,Sg]
  -o, --outdir <outdir>   store results to outdir as json.
  -p, --plotdir <plotdir> save ray path plots in plotdir.
  -r, --ranges            output only arrival ranges for each event.
  -v, --verbose           increase verbosity.
  -h, --help              display this help and exit.
  --version               output version information and exit.

"""
# http://www.isc.ac.uk/standards/phases/
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno
import os.path

import logging

from docopt import docopt
from datetime import datetime

def getStation(opt):
    keys = "lat,lon,elev,depth".split(',')
    values = [ float(field) for field in opt.split(',') ]

    sta = dict(zip(keys, values))
    if 'elev' not in sta:
        sta['elev'] = 0
    if 'depth' not in sta:
        sta['depth'] = 0

    return sta

def getPhaseList(opt):
    return opt.split(',')

from fdsntools.ttlist import travelTimeList
from fdsntools.utils import mkdirs

logging.basicConfig(format='# %(filename)s : %(message)s')
log = logging.getLogger()

if __name__ == "__main__":
#
# test with Ascoli POP preliminary coordinates
# 42° 51.0844’ N     13° 35.3510’
# ==> 42.8514 13.5892
# test command line
# megliott.py -vv -s 42.8514,13.5892 28137841 27776721
#
# check code on https://krischer.github.io/seismo_live_build/html/Rotational%20Seismology/estimate_backazimuth_wrapper.html
#
    try:
        cli = docopt(__doc__, version=__version__)
        #
        # set verbose level
        if cli['--verbose'] >= 2:
            log.setLevel(logging.DEBUG)
        elif cli['--verbose'] == 1:
            log.setLevel(logging.INFO)
        log.debug("cli : %s", str(cli))

        ttl = travelTimeList(
            sta = getStation(cli['--station']),
            eventIDs = cli['<eventID>'],
            service = cli['--fdsn'],
            model = cli['--model'],
            pPhases = getPhaseList(cli['--P-arrivals']),
            sPhases = getPhaseList(cli['--S-arrivals']),
            plotDir = cli['--plotdir']
        )

        ttl.process()

        if cli['--ranges']:
            ttl.getRanges()
        else:
            print(ttl)

        if cli['--outdir']:
            ttl.setOutdir(cli['--outdir'])
            ttl.dump()

    except KeyboardInterrupt:
        log.warning("Program halted")
        sys.exit()
