#!/usr/bin/env python 
# -*- coding: utf-8 -*- 
"""
cat pickle file content to stdout.

Usage: 
  pklcat FILE ...

Arguments:
  FILE	pickle file to cat

Options:
  -h --help              display this help and exit. 
  --version              output version information and exit. 
"""

__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.1.0"
__status__ = "Develop"

import sys, errno, traceback
import os.path
#
from docopt import docopt
import pickle
import pprint

if __name__ == "__main__":
	cargs = docopt(__doc__, version=__version__)
#	print(cargs)
	for pkl in cargs['FILE']:
		try:
			with open(pkl,'rb') as inFile:
				inpkl=pickle.load(inFile)
			print("# %s" % pkl)
			pprint.pprint(inpkl)
		except pickle.UnpicklingError as e:
			# normal, somewhat expected
			print("# %s : not a pickle file - skipped" % pkl)
			continue
		except (AttributeError,  EOFError, ImportError, IndexError) as e:
			# secondary errors
			print("# %s : %s - skipped" % (pkl, e))
			continue
		except FileNotFoundError as e:
			print("# %s : %s - skipped" % (pkl, e.strerror))
			#print(dir(e))
			continue
		except Exception as e:
			# everything else, possibly fatal
			print(traceback.format_exc(e))
			sys.exit()
