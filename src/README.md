# megliott
Uses obspy (https://github.com/obspy/obspy/wiki) fdsn client to
download recursively seismic event data.

This script has been primarily created to download the data of the AlpArray project from EIDA using new fdsn authentication token.

### Setup
Project developed in a conda virtual env with
* python3
* obspy 1.1.x
* docopt

Basic setup:
```
conda create -n fdsn python=3.7
conda activate fdsn
conda install -c conda-forge obspy
conda install -c anaconda docopt
conda install -c conda-forge basemap-data-hires
conda install -c conda-forge ruamel.yaml

```
All config files are in yml format.
This should be enough to run, in any case a minimal file to build the environment is provided in
file://fdsn-environment.yml
To create the environment just run
```
conda env create --file fdsn-environment.yml
```
