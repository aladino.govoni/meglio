# -*- coding: utf-8 -*-
"""
"""
import logging
log = logging.getLogger(__name__)

logging.getLogger('matplotlib').setLevel(logging.WARNING)

import sys, os
from obspy.taup import TauPyModel

import numpy as np

import cartopy.crs as ccrs
import matplotlib.pyplot as plt
plt.switch_backend('Agg')


def ttMap(event, station, outfile='ttMap.pdf', outdir='./'):

    #ax = plt.axes(projection=ccrs.PlateCarree())
    #42.8514,13.5892
    # ax = plt.axes(projection=ccrs.RotatedPole(
    #     pole_latitude=45,
    #     pole_longitude=180
    #     ))
    ax = plt.axes(projection=ccrs.Robinson())
    #ax = plt.axes(projection=ccrs.Mollweide())
    ax.stock_img()
    ax.coastlines()
    ax.gridlines()

    title = f'({event["eventID"]}) {event["time"].replace(microsecond=0).isoformat()}, {event["magType"]}={event["mag"]}, dist={event["degdist"]:.1f}, baz={event["baz"]["backazimuth"]:.1f}\n{event["descr"]}'
    #ax.figure.suptitle(title)
    ax.set_title(title)
    if event['degdist'] <= 30:
         ax.set_extent([-10,40,30,50])
    # #print(station){'lat': 42.8514, 'lon': 13.5892, 'elev': 0, 'depth': 0}
    ax.plot([event['longitude'], station['lon']], [event['latitude'], station['lat']],
         color='blue', linewidth=2,
         #marker='o',
         transform=ccrs.Geodetic(),
         )
    ax.plot(event['longitude'], event['latitude'], '*', color='red', transform=ccrs.PlateCarree())
    ax.plot(station['lon'], station['lat'], 'v', color='green', transform=ccrs.PlateCarree())

    out = os.path.join(outdir,f'{event["eventID"]}-{outfile}')
    log.info(f'saving ray paths to {out}')
    # Save the plot by calling plt.savefig() BEFORE plt.show()
    plt.savefig(out)
    plt.close('all')

def ttPlot(model, event, phList=("ttbasic",), outfile='ttPlot.pdf', outdir='./'):

        arrivals = model.get_ray_paths(
                source_depth_in_km=event['depth']*0.001,
                distance_in_degree=event['degdist'],
                phase_list = phList)
                #[*self.opt['pPhases'],*self.opt['sPhases']])
                #phase_list=("ttbasic",))
                #phase_list=self.opt['pPhases'])
        title = f'({event["eventID"]}) {event["time"].replace(microsecond=0).isoformat()}, {event["magType"]}={event["mag"]}, dist={event["degdist"]:.1f}, baz={event["baz"]["backazimuth"]:.1f}\n{event["descr"]}'

        if event['degdist'] >= 30:
            fig, ax = plt.subplots(subplot_kw=dict(polar=True))
            ax = arrivals.plot_rays(plot_type='spherical',
                    legend="lower left",
                    label_arrivals=True,
                    plot_all=True,
                    show=False, ax=ax)

            # Annotate regions
            ax.text(0, 0, 'Solid\ninner\ncore',
                    horizontalalignment='center', verticalalignment='center',
                    bbox=dict(facecolor='white', edgecolor='none', alpha=0.7))
            ocr = (model.model.radius_of_planet -
                   (model.model.s_mod.v_mod.iocb_depth +
                    model.model.s_mod.v_mod.cmb_depth) / 2)
            ax.text(np.deg2rad(180), ocr, 'Fluid outer\ncore',
                    horizontalalignment='center', verticalalignment='center',
                    bbox=dict(facecolor='white', edgecolor='none', alpha=0.7))
            mr = model.model.radius_of_planet - model.model.s_mod.v_mod.cmb_depth / 2
            ax.text(np.deg2rad(180), mr, 'Solid mantle',
                    horizontalalignment='center',
                    bbox=dict(facecolor='white', edgecolor='none', alpha=0.7))

            # ax.text(0, 1.2*model.model.radius_of_planet,title,
            #     color='blue',
            #     horizontalalignment='center', verticalalignment='center',
            #     bbox=dict(facecolor='yellow', edgecolor='none', alpha=0.7))

        else:
            #fig, ax = plt.subplots()
            ax = arrivals.plot_rays(plot_type='cartesian',
                    legend=False,
                    label_arrivals=False,
                    plot_all=True,
                    show=False)



        ax.figure.suptitle(title)

        out = os.path.join(outdir,f'{event["eventID"]}-{outfile}')
        log.info(f'saving ray paths to {out}')
        ax.figure.savefig(out)

        plt.close('all')
        return True
