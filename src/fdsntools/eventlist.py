# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, os
import logging
log = logging.getLogger(__name__)

from fdsntools.utils import printf
from fdsntools.utils import mkdirs, storeInfo
from fdsntools.utils import fname
from fdsntools.utils import condaGetEnvironmentName

try:
    from obspy import read_inventory, read_events
    from obspy.clients.fdsn import Client
    from obspy import UTCDateTime

    from obspy.geodetics import gps2dist_azimuth, kilometers2degrees
    #from obspy.geodetics import degrees2kilometers
    from obspy.taup import TauPyModel
    from obspy.taup.taup_geo import calc_dist
except ImportError:
    log.debug('Import Error', exc_info=True)

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

import pickle

class EventList:
	"""
	Manage a seismic event list with all needed additional data.
	Create an event list directory with waveforms and travel times.
	"""
	def __init__(self,
		tag="EventList", basedir="./EventList.d",
		verbose=False,
		eida="INGV",
		datapath='data',
		eventKey='Id', # either Id or Time,
		config=None,
		cli=None
		):
		# get some execution info
		self.pwd = os.getcwd()
		# reference stations
		self.referenceStation = None
		log.info("%s : active",__name__)
		#
		if cli is not None:
			self.cli=cli
			log.info("%s : using cli arguments", __name__)
			self.cli['modules']=[]
			self.cli['modules'].append(__name__)
		
		# if a config file has been read use it
		if config is not None:
			self.Config(basedir,config)
		else:
			# setup a minimal default configuration
			self.tag = tag
			self.basedir = basedir
			self.verbose = verbose
			#

			log.debug("current conda environment is '%s'", condaGetEnvironmentName())

			outdir = '/'.join([self.basedir,self.tag])
			mkdirs(outdir)
			self.listdir = os.path.abspath(outdir)
			#print(type(self).__name__)
			log.debug(" %s : current dir is %s",type(self).__name__, self.listdir)
			#
			self.dc = Client(eida)
			#

			#

			#
			#### other useful constants
			self.etc = 'etc'
			self.inventory = 'inventory'
			self.archive = 'archive'
