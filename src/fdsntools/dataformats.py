# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.2"
__status__ = "Develop"

import sys, os
from pathlib import Path
import logging
log = logging.getLogger(__name__)

import h5py
import numpy as np

from obspy import UTCDateTime
from obspy.io.sac.sactrace import SACTrace
# https://noise-python.readthedocs.io/en/latest/examples/source_receiver_geometry.html

from fdsntools.ttlist import travelTimeList
from fdsntools.utils import mkdirs

class hdf5FormatError(Exception):
    """Base class for INRIM data format error"""
    pass

class hdf2sac:
    """
    Read hdf5 data from INRIM and convert/save to sac data format.
    """
    def __init__(self,
        inFiles=None,       # list of files to convert
        code=None,          # station/channell code as nn.sss[ss].ll.ccc
        coord=None,         # site coordinates as lat,lon,elev[,depth]
        outdir='.',
        tag=False, eventID=False, annotate=False):

        if inFiles is not None:
            log.debug("hdf2sac(%s)", ' '.join(inFiles))
            self.inFiles = inFiles

        # new version vars
        self.attr = {}      # dictionary for attributes
        self.cVisit = 0     # debug counter for hdf iterator
        self.header = {}    # sac header basic dictionary

        self.setStation(code)
        self.setCoordinates(coord)
        self._dump_header()

        self.setOutdir(outdir, tag, eventID)
        self.annotate = annotate

    def setStation(self, code):
        if code is not None:
            self._getStationDict(code)

    def setCoordinates(self, coord):
        if coord is not None:
            self._getCoordDict(coord)

    def setOutdir(self, outdir, tag, eventID):
        if outdir is not None:
            self.outdir = [outdir, None, None]
            self.level =1
            if tag:
                self.outdir[1] = ''
            if eventID:
                self.outdir[2] = ''
            #mkdirs(outdir)

    def getOutdir(self):
        out = [x for x in self.outdir if x is not None]
        outdir = os.path.join('', *out)
        mkdirs(outdir)
        return outdir

    def setOutdirTag(self, tag):
        if self.outdir[1] is not None:
            self.outdir[1] = tag
            log.debug("setOutdirTag() : tag set to %s",tag)

    def setOutdirID(self, eventID):
        if self.outdir[2] is not None:
            self.outdir[2] = str(eventID)
            log.debug("setOutdirID() : eventID set to %s", eventID)

    def _dump_header(self, head=None):
        if head is None:
            head = self.header
        for key, val in head.items():
            log.debug("HEADER : %s = %s", key, val)

    def _getStationDict(self, name):
        keys = "knetwk.kstnm.kinst.kcmpnm".split('.')
        values = name.split('.')

        self.header = {**self.header, **dict(zip(keys, values))}

    def _getCoordDict(self, opt):
        keys = "stla,stlo,stel,stdp".split(',')
        values = [ float(field) for field in opt.split(',') ]

        self.header = {**self.header, **dict(zip(keys, values))}

        # write coord dict needed for ttlist (must fix this)
        keys = "lat,lon,elev,depth".split(',')
        self.sta = dict(zip(keys, values))
        if 'elev' not in self.sta:
            self.sta['elev'] = 0
        if 'depth' not in self.sta:
            self.sta['depth'] = 0

    def saveAll(self):
        """"""
        for self.inFile in self.inFiles:
            self.setOutdirTag(Path(self.inFile).stem.upper())
            try:
                with h5py.File(self.inFile, "r") as hf:
                    hf.visititems(self._hdf_visit)

            except FileNotFoundError:
                log.warning("%s : no such file - skipped",self.inFile)

            except KeyError as error:
                # try old format
                log.warning("%s : trying old format", self.inFile)
                with h5py.File(self.inFile, "r") as hf:
                    self._hdf_read_attrs('root', hf)
                    if 'event' in self.attr:
                        self.attr['event_id'] = self.attr['event'].split('/')[-1]
                    log.debug("root attributes : %s", ' '.join(str(self.attr).split()))
                    hf.visititems(self._hdf_getdata)

            except Exception as error:
                log.exception("%s : %s", self.inFile, error)

    def _hdf_read_attrs(self, name, obj):
        for key, val in obj.attrs.items():
            log.debug("attrs : %s : %s: %s" % (name, key, val))
            self.attr[key] = val

    def _hdf_getdata(self, name, node):
        log.debug("===============%03d==================",self.cVisit)
        self.cVisit += 1

        if isinstance(node, h5py.Dataset):
            log.info("%s : DSET  : %s (%d samples, type %s)", self.inFile, name, node.len(), node.dtype)
            self.setOutdirID(self.attr['event_id'])
            #
            self.saveTrace(node[...])
            # clear attr
            self.attr = {}


    def _hdf_visit(self, name, node):
        log.debug("===============%03d==================",self.cVisit)
        self.cVisit += 1
        #
        if isinstance(node, h5py.Dataset):
            # node is a dataset
            #if 'event_id' in self.attr:
            try:
                # new data format
                print(f"{self.inFile} : {self.attr['event_id']} : EVENT : origin {self.attr['origin_time']}, Mag {self.attr['magnitude']}")

                log.info("%s : DSET  : %s (%d samples, type %s)", self.inFile, name, node.len(), node.dtype)
                self._hdf_read_attrs(name, node)
                self.attr['samples'] = node.len()
                #print(self.attr)
                print(f"{self.inFile} : {self.attr['event_id']} : DATA  : start  {self.attr['start_time']}, {self.attr['samples']} samples")

                log.debug("wave attributes : %s", ' '.join(str(self.attr).split()))
                self.setOutdirID(self.attr['event_id'])
                #
                self.saveTrace(node[...])
                # clear attr
                self.attr = {}

            except KeyError as error:
                #log.exception("%s : KeyError : %s", self.inFile, error)
                log.warning("%s : KeyError", self.inFile)
                raise
                #else:
                # it's old data format
            #    log.debug("OLD DATAFORMAT : name %s, node %s",name,node)
        else:
            # node is a group
            log.debug("GROUP : %s", name)
            self._hdf_read_attrs(name, node)


    def saveTrace(self, trace):
        try:
            head = self.header.copy()
            head['kevnm'] = str(self.attr['event_id'])
            #head['kevnm'] = "pippopippo"
            start = UTCDateTime(self.attr['start_time'])
            head['delta'] = 1./self.attr['data_rate']

            head['nzyear'] = start.year
            head['nzjday'] = start.julday
            head['nzhour'] = start.hour
            head['nzmin']  = start.minute
            head['nzsec']  = start.second
            head['nzmsec'] = int(0.001*start.microsecond+0.5)
            log.debug("setting starttime : %s", start)


            self._dump_header(head)
            #print(type(trace))
            tr = SACTrace(data=trace.astype('float32'), **head)

        except KeyError:
            raise hdf5FormatError('data format error')

        except Exception as error:
                log.exception("saveTrace() : %s", error)

        self._addEventInfo(tr)

        reftime = str(tr.reftime).split('.')[0]
        out = os.path.join(self.getOutdir(),f'{tr.knetwk}.{tr.kstnm}.{tr.kinst}.{tr.kcmpnm}.{reftime}.sac')
        log.info("saving sac file %s", out)
        tr.write(out, byteorder='little')
        if 'origin_time' in self.attr:
            origin_time=UTCDateTime(self.attr['origin_time'])
            log.debug("saveTrace() : %s : origin time %s, reftime %s, diff %s (%s)",
                self.attr['event_id'], origin_time, start, start-origin_time,
                self._getLagTime(origin_time, start))
        print(f"{self.inFile} : {self.attr['event_id']} : DATA  : saved to {out}")

    def _getLagTime(self, t1, t2):
        dt = t2 - t1
        sgn=''
        if dt<0:
            sgn='-'
            dt=-dt
        days = int(dt/3600/24)
        dt -= days*3600*24
        hours = int(dt/3600)
        dt -= hours*3600
        minutes = int(dt/60)
        dt -= minutes*60
        return f'{sgn}{days:d}d{hours:02d}h{minutes:02d}m{dt:09.6f}s'

    def _addEventInfo(self, tr):
        #print(self.sta)
        ttl = travelTimeList(
            sta = self.sta,
            eventIDs = [tr.kevnm],
            model = 'ak135'
        )
        ttl.process()
        #print(ttl)
        evt = ttl.getEventInfo(tr.kevnm)

        # 'baz': {'distance': 527290.052809552, 'azimuth': 357.2692065958605, 'backazimuth': 177.0697358054024},
        # 'degdist': 4.7420333707765865,
        #DIST      527.292             # F Station to event distance (km).
        #AZ        357.269             # F Event to station azimuth (degrees).
        #BAZ       177.07              # F Station to event azimuth (degrees).
        #GCARC     4.74337             # F Station to event great circle arc length (degrees).
        # sac mini job to compute ] small discrepancies - check depth in m o km
        # r file
        # ch LCALDA TRUE
        # wh
        # q
        if evt is not None:
            # set hypocentral location
            tr.evla = evt['latitude']
            tr.evlo = evt['longitude']
            tr.evdp = evt['depth']

            tr.dist = 0.001*evt['baz']['distance']
            tr.az   = evt['baz']['azimuth']
            tr.baz  = evt['baz']['backazimuth']
            tr.gcarc= evt['degdist']

            tr.o = evt['time'] - tr.reftime

            tr.kuser0 = f"{evt['mag']} {evt['magType']}"
            #tr.a = evt[pArrivals][0]
            #print(evt['pArrivals'][0])
            tr.a  = tr.o + evt['pArrivals'][0]['time']
            tr.ka = evt['pArrivals'][0]['name']

            #tr.kuser1 = 'kuser1'
            #tr.kuser2 = 'kuser2'
            #tr.kdatrd = 'kdatrd'
            #tr.kinst  = 'kinst'

            try:
                tr.t0  = tr.o + evt['pArrivals'][1]['time']
                tr.kt0 = evt['pArrivals'][1]['name']

                tr.t1  = tr.o + evt['pArrivals'][2]['time']
                tr.kt1 = evt['pArrivals'][2]['name']

                tr.t2  = tr.o + evt['pArrivals'][3]['time']
                tr.kt2 = evt['pArrivals'][3]['name']

                tr.t3  = tr.o + evt['pArrivals'][4]['time']
                tr.kt3 = evt['pArrivals'][4]['name']

                tr.t4  = tr.o + evt['pArrivals'][-1]['time']
                tr.kt4 = evt['pArrivals'][-1]['name']
            except IndexError:
                log.debug("Only %d P arrivals in list",len(evt['pArrivals']))

            try:
                tr.t5  = tr.o + evt['sArrivals'][0]['time']
                tr.kt5 = evt['sArrivals'][0]['name']

                tr.t6  = tr.o + evt['sArrivals'][1]['time']
                tr.kt6 = evt['sArrivals'][1]['name']

                tr.t7  = tr.o + evt['sArrivals'][2]['time']
                tr.kt7 = evt['sArrivals'][2]['name']

                tr.t8  = tr.o + evt['sArrivals'][3]['time']
                tr.kt8 = evt['sArrivals'][3]['name']

                tr.t9  = tr.o + evt['sArrivals'][4]['time']
                tr.kt9 = evt['sArrivals'][4]['name']

                tr.f = tr.o + evt['sArrivals'][-1]['time']
                tr.kf = evt['sArrivals'][-1]['name']
            except IndexError:
                log.debug("Only %d S arrivals in list",len(evt['sArrivals']))

            self._annotateEvent(evt)

    def _annotateEvent(self, evt):
        if self.annotate:
            outd = self.getOutdir()
            log.debug("_addEventInfo() : annotating event %s to %s", evt['eventID'], outd)
            # add location information
            #print(evt)
            with open(os.path.join(outd,'.location'),'wt') as hf:
                hf.write("EventID|Time|Latitude|Longitude|Depth/Km|mag|magType|descr|distance_km|azimuth|backazimuth|degdist\n")
                hf.write(f"{evt['eventID']}|{evt['time']}|{evt['latitude']}|{evt['longitude']}|{evt['depth']}|{evt['mag']}|{evt['magType']}|{evt['descr']}")
                distance_km=0.001*evt['baz']['distance']
                hf.write(f"|{distance_km:.3f}|{evt['baz']['azimuth']:.1f}|{evt['baz']['backazimuth']:.1f}|{evt['degdist']:.3f}")
                hf.write("\n")
            # add Magnitude
            with open(os.path.join(outd,f".mag_{evt['mag']:.1f}"),'wt') as hf:
                hf.write(f"{evt['mag']}|{evt['magType']}\n")
            # add degdist
            with open(os.path.join(outd,f".deg_{evt['degdist']:.3f}"),'wt') as hf:
                hf.write("distance_km|azimuth|backazimuth|degdist\n")
                hf.write(f"{distance_km:.3f}|{evt['baz']['azimuth']:.1f}|{evt['baz']['backazimuth']:.1f}|{evt['degdist']:.3f}\n")
            #add baz
            with open(os.path.join(outd,f".baz_{evt['baz']['backazimuth']:.1f}"),'wt') as hf:
                hf.write("")
            #add eventID
            with open(os.path.join(outd,f".id_{evt['eventID']}"),'wt') as hf:
                hf.write("")
            #EventID|Time|Latitude|Longitude|Depth/Km|Author|Catalog|Contributor|ContributorID|MagType|Magnitude|MagAuthor|EventLocationName|EventType
            #27723281|2021-07-29T06:15:47.688000|55.5434|-158.066|9.8|SURVEY-INGV-A||||Mwpd|7.9|--|Alaska Peninsula, United States [Sea: United States]|earthquake
# {
#   'eventID': '28225541',
#   'time': UTCDateTime(2021, 9, 8, 1, 47, 49, 961000),
#   'latitude': 17.1633, 'longitude': -99.646, 'depth': 19531.0,
#   'originID': '95176151',
#   'mag': 7.2, 'magType': 'Mwp', 'magID': '102040931',
#   'type': 'region name', 'descr': 'Guerrero, Mexico [Land: Mexico]',
#   'baz': {
#       'distance': 10505703.375916174,
#       'azimuth': 42.51646846143935,
#       'backazimuth': 298.39923348082795
#       },
#   'degdist': 94.48006031334762,
#   'pArrivals': [{'distance': 94.48006031334762, 'incident_angle': 13.837908452833705, 'name': 'P', 'ray_param': 262.72239264852726, 'takeoff_angle': 13.881311714360592, 'time': 798.82420797111467}, {'distance': 94.48006031334762, 'incident_angle': 23.99017440756257, 'name': 'PP', 'ray_param': 446.60707161012914, 'takeoff_angle': 24.068605459128218, 'time': 1026.4150953906553}, {'distance': 94.48006031334762, 'incident_angle': 5.1838629531203964, 'name': 'PKiKP', 'ray_param': 99.247098737812649, 'takeoff_angle': 5.1998473626057784, 'time': 1081.8839600317363}], 'pRange': [798.82420797111467, 1081.8839600317363],
#'sArrivals': [{'distance': 94.48006031334762, 'incident_angle': 15.760095166474905, 'name': 'S', 'ray_param': 500.123544903236, 'takeoff_angle': 15.809824543717744, 'time': 1470.1699864810307}, {'distance': 94.48006031334762, 'incident_angle': 3.2658702602325054, 'name': 'SKiKS', 'ray_param': 104.89930344036402, 'takeoff_angle': 3.2759238718203423, 'time': 1511.6016964227981},
#{'distance': 94.48006031334762, 'incident_angle': 26.320743185680275, 'name': 'SS', 'ray_param': 816.43761584762387, 'takeoff_angle': 26.407932269459746, 'time': 1852.508728152377}], 'sRange': [1470.1699864810307, 1852.508728152377]}



# hdf reference
# https://docs.h5py.org/en/stable/high/attr.html#attributes
# https://docs.h5py.org/en/stable/quick.html
#
    def _old_format(self, hdf):
        self._hdf_read_attrs('root',hf)

        header = {}
        # trace attributes in root
        if 'start_time' in hf.attrs:
            self.start = UTCDateTime(hf.attrs['start_time'])
            header['nzyear'] = self.start.year
            header['nzjday'] = self.start.julday
            header['nzhour'] = self.start.hour
            header['nzmin']  = self.start.minute
            header['nzsec']  = self.start.second
            header['nzmsec'] = int(0.001*self.start.microsecond+0.5)
            log.debug("setting starttime : %s", self.start)

        if 'data_rate' in hf.attrs:
            self.delta = 1./hf.attrs['data_rate']
            header['delta'] = self.delta
            log.debug("setting delta : %g", header['delta'])

        if 'event' in hf.attrs:
            self.event = hf.attrs['event']
            header['kevnm'] = os.path.basename(self.event)

        #print(header)

        for dset in hf:
            self._hdf_read_attrs(dset,hf[dset])
            log.debug("%s : dset : shape : %s", dset, hf[dset].shape)
            log.debug("%s : dset : type  : %s", dset, hf[dset].dtype)

            #https://docs.obspy.org/packages/autogen/obspy.io.sac.sactrace.SACTrace.html
            # np.savetxt("data.dat", np.array(hf.get(dset)).astype('float32'), delimiter="\t")
            tr = SACTrace(data=np.array(hf.get(dset)).astype('float32'), **header)
            # #self.sac.reftime = UTCDateTime(hf.attrs['start_time'])
            # #print(self.sac)
            # pritrc.kevnm)
            #print("##--> time",tr.reftime)
            tr.write("test.sac", byteorder='little')
            self.st.append(tr)


    def save(self, outdir='.', namePattern=None):
        ctr=0
        for tr in self.st:
            out = "test-save-%02d.sac" % ctr
            log.debug("saved to %s",out)
            tr.write(out, byteorder='little')
            reftime = str(tr.reftime).split('.')[0]
            out = os.path.join(outdir,f'{tr.knetwk}.{tr.kstnm}.{tr.kinst}.{tr.kcmpnm}.{reftime}.sac')
            log.debug("test name %s", out)
            mkdirs(outdir)
            #os.makedirs(outdir, exist_ok=True)
            tr.write(out, byteorder='little')

    def setStationCode(self, code):
        # dictionary {'network': 'IV', 'code': 'AP01', 'location': '', 'channel': 'HJZ'}
        # location code is not really used with sac (no standard field to set)
        # Seisgram2k uses kinst for location
        log.debug("setStationCode() :  %s",code)

        for tr in self.st:
            tr.kstnm = code['code']
            tr.kcmpnm = code['channel']
            tr.knetwk = code['network']
            tr.kinst  = code['location']

    def setStationCoordinates(self, coord):
        # dictionary {'lat: , 'lon': , 'elev': ,'depth'}
        log.debug("setStationCoordinates() :  %s",coord)

        for tr in self.st:
            try:
                tr.stla = coord['lat']
                tr.stlo = coord['lon']
                tr.stel = coord['elev']
                tr.stdp = coord['depth']
            except KeyError:
                log.debug("setStationCoordinates() : either elev or depth missing")

        self.sta = coord
        if 'elev' not in coord:
            self.sta['elev'] = 0
        if 'depth' not in coord:
            self.sta['depth'] = 0

    def getEventID(self):
        if self.event is not None:
            return os.path.basename(self.event)

    def getEventInfo(self):
        log.debug("getEventInfo()")
        if self.event is not None:
            Event = [os.path.basename(self.event)]
            print(Event)
            ttl = travelTimeList(
                sta = self.sta,
                eventIDs = Event,
                model = 'ak135'
                )

            ttl.process()
            print(ttl)
            print()

            evt = ttl.getEventInfo(os.path.basename(self.event))
            #print(evt)
# 'baz': {'distance': 527290.052809552, 'azimuth': 357.2692065958605, 'backazimuth': 177.0697358054024},
# 'degdist': 4.7420333707765865,
#DIST      527.292             # F Station to event distance (km).
#AZ        357.269             # F Event to station azimuth (degrees).
#BAZ       177.07              # F Station to event azimuth (degrees).
#GCARC     4.74337             # F Station to event great circle arc length (degrees).
# sac mini job to compute ] small discrepancies - check depth in m o km
# r file
# ch LCALDA TRUE
# wh
# q
            if evt is not None:
                for tr in self.st:
                    # set hypocentral location
                    tr.evla = evt['latitude']
                    tr.evlo = evt['longitude']
                    tr.evdp = evt['depth']

                    tr.dist = 0.001*evt['baz']['distance']
                    tr.az   = evt['baz']['azimuth']
                    tr.baz  = evt['baz']['backazimuth']
                    tr.gcarc= evt['degdist']

                    tr.o = evt['time'] - tr.reftime

                    tr.kuser0 = f"{evt['mag']} {evt['magType']}"
                    #tr.a = evt[pArrivals][0]
                    #print(evt['pArrivals'][0])
                    tr.a  = tr.o + evt['pArrivals'][0]['time']
                    tr.ka = evt['pArrivals'][0]['name']

                    #tr.kuser1 = 'kuser1'
                    #tr.kuser2 = 'kuser2'
                    #tr.kdatrd = 'kdatrd'
                    #tr.kinst  = 'kinst'

                    try:
                        tr.t0  = tr.o + evt['pArrivals'][1]['time']
                        tr.kt0 = evt['pArrivals'][1]['name']

                        tr.t1  = tr.o + evt['pArrivals'][2]['time']
                        tr.kt1 = evt['pArrivals'][2]['name']

                        tr.t2  = tr.o + evt['pArrivals'][3]['time']
                        tr.kt2 = evt['pArrivals'][3]['name']

                        tr.t3  = tr.o + evt['pArrivals'][4]['time']
                        tr.kt3 = evt['pArrivals'][4]['name']

                        tr.t4  = tr.o + evt['pArrivals'][-1]['time']
                        tr.kt4 = evt['pArrivals'][-1]['name']
                    except IndexError:
                        log.debug("Only %d P arrivals in list",len(evt['pArrivals']))

                    try:
                        tr.t5  = tr.o + evt['sArrivals'][0]['time']
                        tr.kt5 = evt['sArrivals'][0]['name']

                        tr.t6  = tr.o + evt['sArrivals'][1]['time']
                        tr.kt6 = evt['sArrivals'][1]['name']

                        tr.t7  = tr.o + evt['sArrivals'][2]['time']
                        tr.kt7 = evt['sArrivals'][2]['name']

                        tr.t8  = tr.o + evt['sArrivals'][3]['time']
                        tr.kt8 = evt['sArrivals'][3]['name']

                        tr.t9  = tr.o + evt['sArrivals'][4]['time']
                        tr.kt9 = evt['sArrivals'][4]['name']

                        tr.f = tr.o + evt['sArrivals'][-1]['time']
                        tr.kf = evt['sArrivals'][-1]['name']
                    except IndexError:
                        log.debug("Only %d S arrivals in list",len(evt['sArrivals']))
