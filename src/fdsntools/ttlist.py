# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, os

import logging
log = logging.getLogger(__name__)

#import obspy
import obspy.clients.fdsn
#import pprint

from obspy.taup import TauPyModel
from obspy.geodetics.base import gps2dist_azimuth, kilometers2degrees

import json

from fdsntools.utils import mkdirs
from fdsntools.ttlistplot import ttPlot
from fdsntools.ttlistplot import ttMap

class travelTimeList:
    """
    Retrieve event data and compute travel times.
    """
    def __init__(self,
        sta,                # station dictionary with lat,lon,elev,depth
        eventIDs,            # list of event IDs
        service='INGV',        # fdsn service name
        model='iasp91',        # taup model
        pPhases=['P','p','Pdiff','PP','PKiKP','PKIKP','Pn','Pg'],        # p arrivals list - very long list P,p,Pdiff,PP,PKiKP,PKIKP,Pn,Pg
        sPhases=['S','s','Sdiff','SS','SKiKS','SKIKS','Sn','Sg'],        # s arrivals list - very long list S,s,Sdiff,SS,SKiKS,SKIKS,Sn,Sg
        dumpDir="./tt_data",    # directory for dumping json data
        plotDir=None            # directory for ray path plots
        ):
        log.debug("travelTimeList()")

        self.opt = {}
        self.opt['sta'] = sta
        self.opt['eventIDs'] = eventIDs
        self.opt['service'] = service
        self.opt['model'] = model
        self.opt['pPhases'] = pPhases
        self.opt['sPhases'] = sPhases
        self.opt['dumpDir'] = dumpDir

        self.opt['plotDir'] = plotDir
        if plotDir is not None:
            mkdirs(plotDir)
            log.debug("creating plotDir : %s", plotDir)

        #print(self.opt)

    def _getEventLine(self, event):
        return '|'.join([
            str(event['eventID']),
            str(event['time']),
            str(event['latitude']),
            str(event['longitude']),
            str(event['depth']),
            str(event['mag']),
            str(event['magType']),
            str(event['descr'])
        ])

    def _getEventDict(self, eventID, origin, mag, descr):
        E = {}
        log.info("event %s", '|'.join([eventID,
            str(origin.time),
            str(origin.latitude),str(origin.longitude),str(origin.depth),
            str(origin.resource_id.id).split("=")[1],
            str(mag.mag), str(mag.magnitude_type),
            str(mag.resource_id.id).split("=")[1],
            descr['text']]))
        E['eventID']=eventID
        E['time']=origin.time
        E['latitude']=origin.latitude
        E['longitude']=origin.longitude
        E['depth']=origin.depth
        E['originID']=origin.resource_id.id.split("=")[1]
        E['mag']=mag.mag
        E['magType']=mag.magnitude_type
        E['magID']=mag.resource_id.id.split("=")[1]

        E['type']=descr['type']
        E['descr']=descr['text']

        return E

    def getEvents(self):
        log.info("FDSN service is %s",str(self.opt['service']))
        client = obspy.clients.fdsn.Client(self.opt['service'])
        self.events = []

        for eventID in self.opt['eventIDs']:
            log.info("%s : checking event", eventID)
            try:
                events = client.get_events(eventid=eventID)
                for evt in events:
                    origin = evt.preferred_origin()
                    mag = evt.preferred_magnitude()

                self.events.append(self._getEventDict(eventID, origin, mag, evt.event_descriptions[0]))

            except Exception as error:
                log.exception("%s : %s", eventID,error)


    def getBAZ(self, event):
        baz = gps2dist_azimuth(event['latitude'], event['longitude'], self.opt['sta']['lat'], self.opt['sta']['lon'])

        event['baz']={}
        event['baz']['distance']=baz[0]        # [m]
        event['baz']['azimuth']=baz[1]        # [deg]
        event['baz']['backazimuth']=baz[2]    # [deg]
        #print(baz)

    def getBAZline(self, event):
        return '|'.join([
            str(event['baz']['distance']),
            str(event['baz']['backazimuth'])
        ])

#Distance Depth Phase Travel   Ray Param Takeoff Incident Purist   Purist
#(deg)   (km)   Name  Time (s) p (s/deg) (deg)   (deg)    Distance Name

    def _getArrivalDict(self,arrivals):
        AL = []
        for arr in arrivals:
            A = {}
            #pprint.pprint(vars(arr))
            A['distance'] = arr.distance
            A['incident_angle'] = arr.incident_angle
            A['name'] = arr.name
            A['ray_param'] = arr.ray_param
            A['takeoff_angle'] = arr.takeoff_angle
            A['time'] = arr.time

            AL.append(A)
        return AL

    def _getPhaseLine(self, origin_time, arrival):
        return '|'.join([
            arrival['name'],
            str(origin_time+arrival['time']),
            str(arrival['time']),
            str(arrival['takeoff_angle']),
            str(arrival['incident_angle'])
        ])

    def _getArrivalRange(self, arrivals):
        times = []
        for arr in arrivals:
            times.append(arr.time)

        return [min(times), max(times)]

    def getTT(self):
        log.info("TauP model is %s",self.opt['model'])
        TauP = TauPyModel(self.opt['model'])
        for event in self.events:
            self.getBAZ(event)

            # ~ 0.001 * event['baz']['distance'] / 111.11
            event['degdist'] = kilometers2degrees(0.001 * event['baz']['distance'])

            pArrivals = TauP.get_travel_times(
                distance_in_degree = event['degdist'],
                source_depth_in_km = event['depth']*0.001,
                phase_list=self.opt['pPhases'])
            event['pArrivals'] = self._getArrivalDict(pArrivals)
            event['pRange'] = self._getArrivalRange(pArrivals)

            sArrivals = TauP.get_travel_times(
                distance_in_degree = event['degdist'],
                source_depth_in_km = event['depth']*0.001,
                phase_list=self.opt['sPhases'])
            event['sArrivals'] = self._getArrivalDict(sArrivals)
            event['sRange'] = self._getArrivalRange(sArrivals)

            if self.opt['plotDir'] is not None:
                ttPlot(TauP, event, phList=[*self.opt['pPhases'],*self.opt['sPhases']], outdir=self.opt['plotDir'])
                ttMap(event,self.opt['sta'],outdir=self.opt['plotDir'])

#        print(event)

    def process(self):
        log.debug("ttList.process()")
        self.getEvents()
        self.getTT()

#        print(self.events)
        #print(self.opt)

    def __repr__(self):
        ans = []
        for event in self.events:
            #ans.append(self._getEventLine(event))
            for Arr in event['pArrivals']:
                ans.append('|'.join([
                    self._getEventLine(event),
                    self.getBAZline(event),
                    self._getPhaseLine(event['time'],Arr),
                    ])
                )
            for Arr in event['sArrivals']:
                ans.append('|'.join([
                    self._getEventLine(event),
                    self.getBAZline(event),
                    self._getPhaseLine(event['time'],Arr),
                    ])
                )

        return '\n'.join(ans)

    def getRanges(self):
#28137841|2021-08-31T04:14:14.800000Z|527290.052809552|177.0697358054024|2021-08-31T04:15:26.377259Z|2021-08-31T04:37:51.071857Z
        print("eventID|Origin time|distance (m)|backazimuth|first P arrival|last S arrival")
        for event in self.events:
            print('|'.join([
                event['eventID'],
                str(event['time']),
                self.getBAZline(event),
                str(event['time']+min(event['pRange'])),
                str(event['time']+max(event['sRange']))
                ])
            )

    def getEventInfo(self, eventID):
        log.debug("getEventInfo() : %s", eventID)
        for event in self.events:
            if eventID == event['eventID']:
                return event

        return None

    def dump(self):
        out = self.opt.copy()
        del out['eventIDs']
        del out['dumpDir']

        mkdirs(self.opt['dumpDir'])

        for event in self.events:
            outFile=os.path.join(self.opt['dumpDir'],'data-%s.json' % event['eventID'])
            log.info("%s : event dumped to %s" % (event['eventID'],outFile))
            with open(outFile, 'w') as fp:
                json.dump(dict(out, **event), fp,  indent=2, default=str)

    def setOutdir(self, outdir):
        self.opt['dumpDir'] = outdir
