# -*- coding: utf-8 -*-
"""
"""
__author__ = "Aladino Govoni <aladino.govoni@ingv.it"
__version__ = "0.0.1"
__status__ = "Develop"

import sys, os
import shutil
import pickle
#
import logging
log = logging.getLogger(__name__)

from datetime import datetime

from fdsntools.utils import printf
from fdsntools.utils import mkdirs, storeInfo
from fdsntools.utils import fname
from fdsntools.utils import condaGetEnvironmentName

try:
    from configparser import ConfigParser, ExtendedInterpolation
#	from configparser import RawConfigParser
except ImportError:
    from ConfigParser import ConfigParser, ExtendedInterpolation  # ver. < 3.0

# defin EventMgr custom exceptions
class invalidProjectDir(Exception):
   """provided projectdir is invalid"""
   pass

class EventMgr:
	"""
	Manage a seismic event data project
	"""
	def __init__(self, opt=None):
		#
		_me = "%s.%s()" % (self.__class__.__name__,sys._getframe().f_code.co_name)
		log.debug("%s",_me)
		#
		# check cli dictionary
		if opt is not None:
			self.opt=opt
			log.info("%s : using cli arguments", _me)
		else:
			# use default options
			self.opt={}
			log.info("%s : no options, using defaults", _me)

		self.opt['startTime'] = datetime.now()
		log.info("job start %s",self.opt['startTime'])

		if 'modules' not in self.opt:
			self.opt['modules']=[]
		self.opt['modules'].append(__name__)

		self.opt['exec_dir']=os.getcwd()

		self.config = ConfigParser(
#			comment_prefixes='/',
			inline_comment_prefixes='#',
			allow_no_value=True,
			interpolation=ExtendedInterpolation()
			)
		self.projectConfig=".config"

		# first check that project is defined
		self._projectdirSetup()
		log.warning("project dir is %s",os.path.dirname(self.opt['config_file']))

		if not self.opt['cli']['init']:
			self._projectdirProcess()

		self.opt['stopTime'] = datetime.now()
		log.info("job end   %s",self.opt['stopTime'])
		log.info("job exec  %s",self.opt['stopTime']-self.opt['startTime'])

	def _projectdirProcess(self):
		"""process project dir"""
		log.debug("%s.%s()", self.__class__.__name__, sys._getframe().f_code.co_name)

		if self.opt['config']['catalog']:
			log.info('checking catalog')

		if self.opt['cli']['status']:
			log.info("get project status")

	def getStatus(self):
		""" return class status variable"""
		return self.opt

	def saveStatus(self, outfile='.last-run', dump=False):
		"""dump status in project dir"""
		log.debug("%s.%s()", self.__class__.__name__, sys._getframe().f_code.co_name)

		out = os.path.join(self.opt['cli']['--project'], outfile);
		log.info("saving status to %s",out)
		with open(out,'wb') as hf:
			pickle.dump(self.opt, hf)

		if dump:
			self.dumpStatus()

	def dumpStatus(self, indent=2):
		"""dump status on stdout"""
		log.debug("%s.%s()", self.__class__.__name__, sys._getframe().f_code.co_name)

		import pprint
		pprint.pprint(self.opt, indent=indent)

	def isProjectDir(self, path):
		if os.path.isdir(path):
			return True
		else:
			return False

	def _getConfigDictionary(self, src=None):
		"""read config and return a dictionary"""
		log.debug("%s.%s()",self.__class__.__name__,sys._getframe().f_code.co_name)

		default="""\
# default fdsnEventListMgr config file
# autogenarated on %s
# edit to ...

[eida]
node = INGV

[fdsn]
routing = eida-routing
token =

[stations]
# All dates in ISO format YYYY-mm-dd[Thh:mm:ss.sss]
starttime = 2015-06-01
endtime   = 2019-04-01

minlatitude  = 45.01
maxlatitude  = 48.94
minlongitude = 10.25
maxlongitude = 17.06
includerestricted    = True
;includeavailability = True
;matchtimeseries     = True
;level="response"

[list]
tag=%s
# either Id or Time
eventKey='Id'

[catalog]
starttime    = 2016-01-01
endtime      = 2019-04-01
minmagnitude = 6

[paths]
etc       = etc
var       = var
inventory = ${etc}/inventory
archive   = archive
data      = data
plots     = plots
scripts   = bin
""" % (datetime.now(), self.opt['cli']['--tag'])

		if src is not None:
			self.config.read(src)
		else:
			if self.opt['cli']['--config'] is not None:
				self.config.read(self.opt['cli']['--config'])
			else:
				self.config.read_string(default)

		confd = {}
		for sec in self.config.sections():
			confd[sec] = {}
			for key, val in self.config.items(sec):
				confd[sec][key] = val

		return confd


	def _projectdirInit(self):
		"""initialize project dir"""
		_me = "%s.%s()" % (self.__class__.__name__,sys._getframe().f_code.co_name)
		log.debug("%s",_me)

		try:
			os.makedirs(self.opt['cli']['--project'])
			self.opt['config'] = self._getConfigDictionary()

		except OSError as e:
			if os.path.isdir(self.opt['cli']['--project']):
				log.info("%s : %s : project dir already exists",_me,self.opt['cli']['--project'])
				if self.opt['cli']['--force']:
					self._projectdirCleanup()
				else:
					raise invalidProjectDir("use -f to force cleaning")
			else:
				raise invalidProjectDir(str(e))

		self.opt['config_file']=os.path.abspath(os.path.join(self.opt['cli']['--project'],self.projectConfig))
		with open(self.opt['config_file'], 'w') as outfile:    # save
			self.config.write(outfile)

		# create paths
		for entry in self.opt['config']['paths'].values():
			log.debug("%s : mkdir -p %s", _me, os.path.join(self.opt['cli']['--project'],entry))
			os.makedirs(os.path.join(self.opt['cli']['--project'],entry))

		log.warning("%s : project dir created", self.opt['cli']['--project'])

	def _projectdirCleanup(self):
		"cleanup existing project dir"
		_me = "%s.%s()" % (self.__class__.__name__,sys._getframe().f_code.co_name)
		log.debug("%s",_me)
		cfgFile=os.path.join(self.opt['cli']['--project'],self.projectConfig)
		if os.path.isfile(cfgFile):
			self.opt['config'] = self._getConfigDictionary(cfgFile)
		else:
			log.warning("no config file in %s, using defaults",cfgFile)
			self.opt['config'] = self._getConfigDictionary()
		#
		for entry in os.listdir(self.opt['cli']['--project']):
			epath=os.path.join(self.opt['cli']['--project'], entry)
			if os.path.isdir(epath):
				log.debug("%s : rm -rf %s",_me,epath)
				shutil.rmtree(epath)
			else:
				log.debug("%s : rm -f %s",_me,epath)
				os.remove(epath)

	def _projectdirSetup(self):
		"""setup project dir"""
		_me = "%s.%s()" % (self.__class__.__name__,sys._getframe().f_code.co_name)
		log.debug("%s",_me)
		#
		if self.opt['cli']['init']:
			self._projectdirInit()
		else:
			cfgFile=os.path.join(self.opt['cli']['--project'],self.projectConfig)
			if os.path.isfile(cfgFile):
				self.opt['config'] = self._getConfigDictionary(cfgFile)
				self.opt['config_file']=os.path.abspath(cfgFile)
			else:
				raise invalidProjectDir("no project dir found")
