PROJECT := $(notdir $(CURDIR))

.PHONY:

all:
	git add --all
	git commit -m "backup commit"
	git push origin master

add:
	git add --all
	git status

help :
	@echo ""
	@echo "** manage gitlab project"
	@echo " make          : add all files, commit and push to origin"
	@echo " make commit   : add all files and commit"
	@echo " make tag      : tag current version"
	@echo " make push     : push to origin"
	@echo " make pull     : pull from origin"
	@echo ""

commit :
	git add --all
	git commit -m "fix meglio2sac, documentation"

tag:
	git tag -a "v-0.1" -m "initial version, tt computed and saved (given to INRIM)"

push:
	git push origin master

pull:
	git pull

environment:
	conda env export | grep -v "^prefix:" > $(PROJECT)-environment-full.yml
	conda env export  --from-history | grep -v "^prefix:" > $(PROJECT)-environment.yml

check:
	@echo "project name : $(PROJECT)"
